# Copyright Julian M. Kleber 

echo "local-ci  Copyright (C) 2022 Julian M. Kleber This program comes with ABSOLUTELY NO WARRANTY; for details type 'show w'. This is free software, and you are welcome to redistribute it under certain conditions; type 'show c' for details."

black weather_collector/
find . -type f -name '*.txt' -exec sed --in-place 's/[[:space:]]\+$//' {} \+ #sanitize trailing whitespace
autopep8 --in-place --recursive weather_collector/
python -m flake8 weather_collector/ --count --select=E9,F63,F7,F82 --show-source --statistics
mypy --strict weather_collector/
python -m pylint -f parseable weather_collector/
python -m pytest tests/