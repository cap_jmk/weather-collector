"""
Module for formatting Python files

:author: Julian M. Kleber
"""
import os

import click

from bulkhead.sampler.sampler import SamplerEndpoints


@click.command()
@click.option("-o", help="Output file name")
@click.option("-i", help="Output file name")
def sample(o: str, i: float) -> None:
    """
    The sample function is a wrapper for the sample_infinite_intervall function.
    It takes in an output file path, and an interval as parameters. The output file
    path is used to create a directory where the sampled data will be saved, and
    the interval is used to determine how often sampling should occur. The sample
    wrapper function then calls the sample_infinite_intervall function with these two
    parameters along with some other parameters that are specific to this project.

    :param o:str: Used to Define the output file name and location.
    :param i:float: Used to Set the interval in seconds.
    :return: A nonetype object.

    :doc-author: Julian M. Kleber
    """

    save_dir = os.path.dirname(o)
    file_name = os.path.basename(o)
    header = ["time_stamp", "Tomorrow.io", "OpenWeatherMap", "mean"]
    ec = weather_collection
    value_name = "cloud_cover"
    method_name = "get_time_mean_atomic_vals"
    interval = i

    sampler = SamplerEndpoints(object_instance=ec)
    method_parameters = {"value_name": value_name}

    sample_infinite_intervall(
        file_name=file_name,
        save_dir=save_dir,
        header=header,
        method_name=method_name,
        interval=interval,
        sampler=sampler,
    )


def sample_infinite_intervall(**kwargs: Any) -> None:
    """
    The sample_infinite_intervall function samples the data from a given interval and saves it to a csv file.
        The function is called in an infinite loop, so that the sampling can be done continuously.

    :param **kwargs:Any: Used to Pass in a dictionary of arguments.
    :return: Nothing.

    :doc-author: Julian M. Kleber
    """
    file_name = str(kwargs["file_name"])
    save_dir = str(kwargs["save_dir"])
    header = list(kwargs["header"])
    method_name = str(kwargs["method_name"])
    interval = int(kwargs["interval"])
    sampler = kwargs["sampler"]

    while True:
        try:
            data = sampler.sample_intervall(method_name=method_name, interval=0, num=1)
            sampler.save_to_csv(save_dir, file_name, data, header)
        except:
            continue
        time.sleep(interval)


if __name__ == "__main__":
    sample()
